'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Url = require('./url'),
    Schema = mongoose.Schema;

/**
 * Stats Schema
 */
var StatsSchema = new Schema({
    id: {
        type: String,
        required: true,
        unique: true
    },
    hits: {
        type: Number,
        default: 0
    },
    url: {
        type: String
    },
    shortUrl: {
        type: String
    },
    created_at: {
        type: Date,
        default: Date.now
    },
    updated_at: {
        type: Date,
        default: Date.now
    }
},{ versionKey: false });

/**
 * Methods
 */
StatsSchema.methods = {

    buildStatsReport: function (callback) {

        Url.aggregate([
            {
                $project : {
                    _id: 1,
                    hits : 1,
                    url: 1,
                    shortUrl: 1
                }
            },
            {
                $sort: {
                    hits: -1
                }
            },
            {
                $group: {
                    _id: "$id",
                    totalHits: {
                        $sum: "$hits"
                    },
                    urlCount: {
                        $sum: 1
                    },
                    urls: {
                        $push:  {
                            _id: "$_id",
                            hits: "$hits",
                            url: "$url",
                            shortUrl: "$shortUrl"
                        }
                    }
                }
            },
            {
                $project: {
                    _id: 0,
                    hits: "$totalHits",
                    urlCount: "$urlCount",
                    urls: { "$slice": [ "$urls", 10 ] }
                }
            }
        ], function (err, result) {
            if (err) {
                callback(err, null);
            }

            callback(null, result);
        });
    }
};

module.exports = mongoose.model('Stats', StatsSchema);