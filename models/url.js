'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    shortId = require('shortid'),
    Schema = mongoose.Schema;

/**
 * Url Schema
 */
var UrlSchema = new Schema({
    url: {
        type: String,
        required: true
    },
    shortUrl: {
        type: String,
        unique: true,
        required: false,
        default: shortId.generate
    },
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: false
    },
    hits: {
        type: Number,
        default: 0
    },
    created_at: {
        type: Date,
        default: Date.now
    },
    updated_at: {
        type: Date,
        default: Date.now
    }
},{ versionKey: false });

module.exports = mongoose.model('Url', UrlSchema);