'use strict';

var mongoose = require('mongoose'),
    Stats = require('../models/stats'),
    Url = require('../models/url');

var StatsController = {

    index: function( req, res, next ) {
        res.setHeader('Access-Control-Allow-Origin','*');

        var stats = new Stats();
        stats.buildStatsReport(function (err, statsReport) {
            if (err) {
                return next(err);
            }
            res.send(200, statsReport);
        });
    },

    show: function( req, res, next ) {
        res.setHeader('Access-Control-Allow-Origin','*');
        Url.find({ '_id': req.params.id },
            function(err, data) {
                if (!!err){
                    return next(err);
                } else if(!data) {
                    res.send(404);
                } else {
                    var response = [];
                    data.forEach(function (url) {
                        var stats = new Stats(url);
                        stats.id = url._id;
                        stats.shortUrl = "http://short.url/" + url.shortUrl;
                        stats.created_at = undefined;
                        stats.updated_at = undefined;
                        stats._id = undefined;
                        response.push(stats);
                    });
                    res.send(200 , response);
                    next();
                }
            }
        );
    }
};

module.exports = StatsController;