'use strict';

var mongoose = require('mongoose'),
    User = require('../models/user'),
    Stats = require('../models/stats'),
    Url = require('../models/url');

var UserController = {

    show: function( req, res, next ) {
        res.setHeader('Access-Control-Allow-Origin','*');

        var userId = req.params.userid;
        User.findOne({'id': userId}, function(err, user) {
            if (!!err) {
                return next(err);
            } else if(!user) {
                res.send(404);
            }

            Url.find({'userId': user._id}, function(err, data) {
                if (!!err) {
                    return next(err);
                } else {
                    var response = [];
                    data.forEach(function (url) {
                        var stats = new Stats(url);
                        stats.id = url._id;
                        stats.shortUrl = "http://short.url/" + url.shortUrl;
                        stats.created_at = undefined;
                        stats.updated_at = undefined;
                        stats._id = undefined;
                        response.push(stats);
                    });
                    res.send(200, response);
                    next();
                }
            })

        });
    },

    create: function( req, res, next ) {
        res.setHeader('Access-Control-Allow-Origin','*');

        var user = new User(req.body);

        user.save(function(err  ) {
            if (!!err) {
                return next(err);
            } else {
                res.send(201);
                next();
            }
        })
    },

    destroy: function( req, res, next ) {
        res.setHeader('Access-Control-Allow-Origin','*');

        User.findOneAndRemove({'id': req.params.userId }, function(err) {
            if (!!err) {
                return next(err);
            } else {
                res.send(204);
                next();
            }
        });
    }
};

module.exports = UserController;