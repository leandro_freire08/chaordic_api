'use strict';

var mongoose = require('mongoose'),
    Url = require('../models/url'),
    User = require('../models/user'),
    Stats = require('../models/stats');

var UrlController = {

    show: function( req, res, next ) {
        res.setHeader('Access-Control-Allow-Origin','*');
        Url.findOneAndUpdate({ '_id': req.params.id }, { $inc: { hits: 1 } },
            function(err, data) {
                if (!!err){
                    return next(err);
                } else if(!data) {
                    res.send(404);
                } else {
                    res.redirect(301, data.url, next);
                }
            }
        );
    },

    create: function( req, res, next ) {
        res.setHeader('Access-Control-Allow-Origin','*');

        var userId = req.params.userid;

        User.findOne({'id': userId}, function(err, user) {
            if (!!err) {
                return next(err);
            } else if (!user) {
                res.send(404);
            }

            var url = new Url({
                url: req.body.url,
                userId: user._id
            });

            url.save(function(err, data) {
                if (!!err) {
                    return next(err);
                } else {
                    var stats = new Stats(data);
                    stats.id = data.id;
                    stats.shortUrl = "http://short.url/" + data.shortUrl;
                    stats.created_at = undefined;
                    stats.updated_at = undefined;
                    stats._id = undefined;
                    res.send(201, stats);
                    next();
                }
            })

        });
    },

    destroy: function( req, res, next ) {
        res.setHeader('Access-Control-Allow-Origin','*');

        Url.findOneAndRemove({'_id': req.params.id }, function(err) {
            if (!!err) {
                return next(err);
            } else {
                res.send(204);
                next();
            }
        });
    }
};

module.exports = UrlController;