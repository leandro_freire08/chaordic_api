# URL Shorter API

### Dependencies

- [Node.js >= 4.x](http://nodejs.org)
- [MongoDB >= 3.2.x](<https://www.mongodb.org)

### Installation

```sh
$ git clone git@bitbucket.org:leandro_freire08/chaordic_api.git
$ cd chaordic_api
$ npm install
```
Don't forget to set environment variable NODE_ENV to specify which env
the application is running on._

## Configuring

See `config/default.json5.example` for a sample to get you started. 
If you need to change any defaults, make a copy named `default.json5`
and change it to your
liking.

If you want to have different configuration properties for different
environments, create configuration files named after the environments 
they are for. 
For example, to create a configuration file that will be used when
`NODE_ENV` is `development`, create `development.json5`.

## Starting and stopping

Running `npm run start` will start the server using [forever](
https://github.com/nodejitsu/forever), and running `npm run stop` will stop it.
`npm run status` will list the forever processes that are running.

### Testing

```sh
$ npm run test
```

### Considerations

There is an endpoint "/" that contains a list of all available endpoints
at API.
