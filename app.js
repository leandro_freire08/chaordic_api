/* global process:true, __dirname:true */

'use strict';

var path    = require('path'),
    restify = require('restify'),
    config  = require('config'),
    Router = require('paper-router'),
    mongoose = require('mongoose');

exports.createServer = createServer;

/*
 * Set up server
 * @return the created server
 */
function createServer (logger) {

    var settings = {
    name: (config.has('server.name') && config.get('server.name'))
            ? config.get('server.name')
            : require(path.join(__dirname, 'package')).name
    };

    if (logger) {
      settings.log = logger;
    }

    var server = restify.createServer(settings);


    var allowedHeaders = function(req, res, next) {
      res.header('Access-Control-Allow-Origin', '*');
      res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
      res.header('Access-Control-Allow-Headers', 'Origin, Content-Type, Accept');
      next();
    };

    server.use(restify.acceptParser(server.acceptable));
    server.use(restify.queryParser());
    server.use(restify.jsonp());
    server.use(restify.bodyParser({ mapParams: false }));
    server.use(allowedHeaders);
    server.use(restify.pre.sanitizePath());

    if (logger) {
        server.on('after', restify.auditLogger({ log: logger }));
    }

    mongoose.connect(config.get('db.url'));

    // API
    server.get("/", function (req, res) {
      res.send(200, server.router.mounts);
    });

    // Resource Routes
    var routes = function( router ) {
        router.get('/url/:id', 'url#show');
        router.del('/url/:id', 'url#destroy');
        router.get('/stats', 'stats#index');
        router.get('/stats/:id', 'stats#show');
        router.post('/user', 'user#create');
        router.post('/user/:userid/urls', 'url#create');
        router.get('/user/:userid/stats', 'user#show');
        router.del('/user/:userId', 'user#destroy');
    };

    server.on('NotFound', function (req, res) {
        if (logger) {
            logger.debug('404', 'No route that matches request for ' + req.url);
        }
        res.send(404, req.url + ' was not found');
    });

    new Router(server, path.join( __dirname, '/controllers' ), routes);

    return server;
}
