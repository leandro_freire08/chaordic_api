/* global describe:true, before:true, after:true, it:true, global:true,
 baseURL:true, process:true, urlId:true */

var config       = require('config'),
    app          = require('../app'),
    bunyan       = require('bunyan'),
    PrettyStream = require('bunyan-prettystream'),
    request      = require('supertest'),
    mongoose     = require('mongoose'),
    User  = require('../models/user'),
    Url  = require('../models/url');

var server;

before(function (done) {

    var mongoId = mongoose.Types.ObjectId();

    var userData = {
        _id: mongoId,
        id: 'test_user'
    };

    var urlData = {
        _id: mongoose.Types.ObjectId(),
        url: 'http://test.test',
        userId: mongoId
    };

    var userModel = new User(userData);
    userModel.save(function(err, data) {});

    var urlModel = new Url(urlData);
    urlModel.save(function(err, data) {
        global.urlId = data._id;
    });

    var anotherUrlData = {
        _id: mongoose.Types.ObjectId(),
        url: 'http://test.test',
        userId: mongoId,
        hits: 2
    };

    urlModel = new Url(anotherUrlData);
    urlModel.save(function(err, data) {});

    var bunyanToConsole = new PrettyStream();
    bunyanToConsole.pipe(process.stdout);

    var logger = bunyan.createLogger({
        name: 'testLogger',
        streams: [{
            level: 'error',
            type: 'raw',
            stream: bunyanToConsole
        }]
    });

    server = app.createServer(logger);

// start listening
    var port = config.get('server.port');
    server.listen(port, function () {
        logger.info('%s listening at %s', server.name, server.url);
    });

    global.baseURL = 'http://localhost:' + port;

// make sure the server is started
    setTimeout(function() {
        request(baseURL)
            .get('/')
            .end(function (err, res) {
                if (err && err.code === 'ECONNREFUSED') {
                    return done(new Error('Server is not running.'));
                }
                return done(err);
            });
    }, 500);
});

after(function (done) {
    mongoose.connection.db.dropCollection('users');
    mongoose.connection.db.dropCollection('urls');
    mongoose.connection.db.dropCollection('stats');
    done();
    server.close();
});
