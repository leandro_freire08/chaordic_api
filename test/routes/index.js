/* global describe:true, before:true, after:true, it:true, baseURL:true */

'use strict';

var should  = require('should'),
    request = require('supertest');


describe("Endpoint '/'", function () {

  it('should return a list of endpoints', function (done) {
    request(baseURL)
      .get('/')
      .set('Accept', 'application/json')
      .expect('Content-Type', 'application/json')
      .expect(200)
      .end(function (err, res) {
        if (err) return done(err);
        res.body.should.be.an.Object();
        
        return done();
      });
  });

});
