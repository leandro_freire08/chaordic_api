/* global describe:true, before:true, after:true, it:true, baseURL:true, urlId:true */

var should = require('should');
var assert = require('assert');
var request = require('supertest');
var mongoose = require('mongoose');
var config = require('config');

describe("Endpoint: '/stats'", function() {

    it('should return an url stats', function(done) {
        request(baseURL)
            .get('/stats/'+ urlId)
            .end(function(err, res) {
                if (err) {
                    throw err;
                }
                res.body.should.be.an.Array();

                var data = res.body[0];

                data.should.be.an.Object();
                data.should.have.property('id');
                data.should.have.property('url');
                data.should.have.property('shortUrl');
                data.should.have.property('hits');
                res.status.should.equal(200);
                done();
            });
    });

    it('should return stats for all urls', function(done) {
        request(baseURL)
            .get('/stats')
            .end(function(err, res) {
                if (err) {
                    throw err;
                }
                res.body.should.be.an.Array();

                var data = res.body[0];
                data.should.be.an.Object();
                data.should.have.property('urlCount');
                data.urlCount.should.be.equal(2);
                data.should.have.property('urls');
                var urls = data.urls;
                urls.should.be.an.Array();
                urls.length.should.be.equal(2);
                data.should.have.property('hits');
                data.hits.should.be.equal(2);
                res.status.should.equal(200);
                done();
            });
    });
});