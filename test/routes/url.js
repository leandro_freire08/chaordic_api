/* global describe:true, before:true, after:true, it:true, baseURL:true, urlId:true */

var should = require('should');
var assert = require('assert');
var request = require('supertest');
var mongoose = require('mongoose');
var config = require('config');
var Url = require('../../models/url');

describe("Endpoint: '/url'", function() {

    it('should redirect to shortUrl', function(done) {
        request(baseURL)
            .get('/url/'+ urlId)
            .end(function(err, res) {
                if (err) {
                    throw err;
                }
                res.status.should.equal(301);

                Url.findOne({ '_id': urlId }, function(err, data) {
                    data.hits.should.be.equal(1);
                });

                done();
            });
    });

    it('should create a new short-url', function(done) {
        request(baseURL)
            .post('/user/test_user/urls')
            .send({"url": "http://test.com"})
            .end(function(err, res) {
                if (err) {
                    throw err;
                }
                res.status.should.equal(201);
                var data = res.body;
                data.should.be.an.Object();
                data.should.have.property('id');
                data.should.have.property('url');
                data.should.have.property('shortUrl');
                data.should.have.property('hits');

                done();
            });
    });

    it('should return 404 for an user that does not exist', function(done) {
        request(baseURL)
            .post('/user/test_user_2/urls')
            .send({"url": "http://test.com"})
            .end(function(err, res) {
                if (err) {
                    throw err;
                }
                res.status.should.equal(404);

                done();
            });
    });

    it('should delete an url', function(done) {
        request(baseURL)
            .delete('/url/' + urlId)
            .end(function(err, res) {
                if (err) {
                    throw err;
                }

                res.status.should.equal(204);
                done();
            });
    });
});