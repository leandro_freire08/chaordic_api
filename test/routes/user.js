/* global describe:true, before:true, after:true, it:true, baseURL:true */

var should = require('should');
var assert = require('assert');
var request = require('supertest');
var mongoose = require('mongoose');
var config = require('config');

describe("Endpoint: '/user'", function() {

    var profile = {
        id: 'test_user'
    };

    it('should create a new user', function(done) {
        request(baseURL)
            .post('/user')
            .send(profile)
            .end(function(err, res) {
                if (err) {
                    throw err;
                }

                res.status.should.equal(201);
                done();
            });
    });

    it('should return an user by id', function(done) {
        request(baseURL)
            .get('/user/'+ profile.id + '/stats')
            .end(function(err, res) {
                if (err) {
                    throw err;
                }
                res.body.should.be.an.Array();

                var data = res.body[0];

                data.should.be.an.Object();
                data.should.have.property('id');
                data.should.have.property('url');
                data.should.have.property('shortUrl');
                data.should.have.property('hits');
                res.status.should.equal(200);
                done();
            });
    });

    it('should return 404 for an user that does not exist', function(done) {
        request(baseURL)
            .get('/user/testx/stats')
            .end(function(err, res) {
                if (err) {
                    throw err;
                }

                res.status.should.equal(404);
                done();
            });
    });

    it('should delete an user', function(done) {
        request(baseURL)
            .delete('/user/' + profile.id)
            .end(function(err, res) {
                if (err) {
                    throw err;
                }

                res.status.should.equal(204);
                done();
            });
    });
});